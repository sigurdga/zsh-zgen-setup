My Zgen configuration
=====================

This script is sourced in .zshrc, and will set up the .zgen/init.zsh
initialization file.

You should put `skip_global_compinit=1` in your .zshenv

And add `source ~/path/to/this/file` to your .zshrc to load this file.

Zgen documentation is at <https://github.com/tarjoilija/zgen>
